﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="postpage.aspx.cs" Inherits="SimilarityForm1.postpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SimilarityForm | Postpage</title>
      <link href="css/animate.css" rel="stylesheet" type="text/css" />
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

      <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
      <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="">

    <form id="form1" runat="server">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <%--<img alt="image" class="rounded-circle" src="img/profile_small.jpg"/>--%>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                           <%-- <span class="block m-t-xs font-bold">David Williams</span>
                            <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>--%>
                        </a>
                      <%--  <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                            <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                            <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="login.html">Logout</a></li>
                        </ul>--%>
                    </div>
                    <div class="logo-element">
                       <%-- Welcome--%>
                    </div>
                </li>
               
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
              <%--  <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>--%>
        </div>
        
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message"></span>
                </li>
                <li class="dropdown">
                        <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="image" class="rounded-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="float-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>


                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="image" class="rounded-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="float-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>

                           
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="image" class="rounded-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="float-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html" class="dropdown-item">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    &nbsp;</a>
                    <asp:Button 
                        ID="Button3" runat="server" Text="Return" Height="22px" 
                        Width="62px" BackColor="#999999" ForeColor="White" OnClick="Button3_Click"></asp:Button>

            </ul>

        </nav>
        </div>
        <asp:HiddenField ID="hfTrue" runat="server" />
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                  <%--  <h2>This is main title</h2>--%>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <%--<a href="index.html">This is</a>--%>
                        </li>
                        <li class="breadcrumb-item active">
                            <%--<strong>Breadcrumb</strong>--%>
                        </li>
                    </ol>
                   
                </div>
              
            </div>
         

             <div class="header">
                <div class="top-right">
                 <%--   10GB of <strong>250GB</strong> Free.--%>
                </div>
                <div>
                
                    <%--<strong>Copyright</strong> Example Company &copy; 2014-2018--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ID<asp:TextBox ID="TextBox1" 
                        runat="server" Width="209px" Height="40px"></asp:TextBox>
                &nbsp;&nbsp;
                    <br />
                    <br />
                    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    text<textarea id="TextArea1" runat="server" cols="20" name="S1" rows="2"></textarea><br />
                    <br />
&nbsp;&nbsp;
                    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Post" 
                        Width="749px" BackColor="#4CAF50" />
                    <br />
                    <br />
                    <br />
                </div>
               
               


         <%--   </div>

            <div>
            <%if (Session["Name"] != null)
              {%> 
            <%} %>
            </div>
            
          <% if(hfTrue.Value=="1"){
                 int i=0;
                 while(i<obb.DT.Rows.Count){%>  
            <h2><%=obb.DT.Rows[i][0].ToString() %></h2>
            <p><%=obb.DT.Rows[i][1].ToString() %></p>
           <%i++;}} %>
           

        </div>--%>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
         

    <!-- Mainly scripts -->
    
    <script src="js/bootstrap.js" type="text/javascript"></script>
    <script src="js/inspinia.js" type="text/javascript"></script>
    <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.metisMenu.js" type="text/javascript"></script>
    <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="js/pace.min.js" type="text/javascript"></script>
    <script src="js/popper.min.js" type="text/javascript"></script>
    </form>
</body>
</html>
