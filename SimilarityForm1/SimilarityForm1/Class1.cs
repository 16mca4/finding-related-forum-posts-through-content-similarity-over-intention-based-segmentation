﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
namespace SimilarityForm1
{
    public class Class1
    {


        public SqlCommand sqlcommand;
        public DataSet ds = new DataSet();
        public static string connstring = System.Configuration.ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;
        public SqlConnection conn = new SqlConnection(connstring);
        public SqlDataAdapter DA;
        public DataTable DT = new DataTable();



        public void ExecuteQuery(string strquery)
        {
            SqlCommand command = new SqlCommand(strquery, conn);
            SqlDataAdapter ODA = new SqlDataAdapter(command);
            try
            {
                ODA.Fill(ds);
                ODA.Fill(DT);
            }
            catch (Exception ex) { }
            conn.Close();
        }

        public void ExecuteNonQuery(string strquery)
        {
            try
            {
                conn.Open();
                SqlCommand OSC = new SqlCommand(strquery, conn);
                OSC.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            conn.Close();
        }






    }
}