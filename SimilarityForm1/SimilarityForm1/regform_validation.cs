﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace SimilarityForm1
{
    public class regform_validation
    {
        public String error;
        public Boolean check_name(string txtName)
        {

            if (txtName == "")
            {
                error = "Name cannot be null";
                return false;
            }
            else if (txtName.Length < 4)
            {
                error = "Name should contain minimum 4 letters ";
                return false;
            }
            else
                return true;
        }
        public Boolean check_mail(string txtmail)
        {
            if (txtmail == "")
            {
                error = "Mail cannot be null";
                return false;
            }
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(txtmail);
            if (!match.Success)
            {

                error = "Enter Valid Email Address";
                return false;
            }
            else
                return true;
        }
        public Boolean check_mobile(string txtMobile)
        {
            if (txtMobile == "")
            {
                error = "Mobile number cannot be null";
                return false;
            }
            if ((txtMobile.Length < 10) || (txtMobile.Length > 10))
            {
                error = "Enter 10 digits ";
                return false;
            }
            Regex regex = new Regex(@"^[9876]{1}[0-9]{9}$");
            Match match = regex.Match(txtMobile);
            if (!match.Success)
            {
                error = "Enter correct mobile number";
                return false;
            }
            else
                return true;
        }
        public Boolean check_age(string txtAge, int min = 18, int max = 50)
        {
            if (txtAge == "")
            {
                error = "Age cannot be null";
                return false;
            }
            int number;
            if (!(int.TryParse(txtAge, out number)))
            {
                error = "enter Valid age";
                return false;
            }
            else if ((number < min) || (number > max))
            {
                error = "Age must be between 18 and 50";
                return false;
            }
            else
                return true;

        }

        public Boolean check_password(string txtPassword)
        {
            if (txtPassword == "")
            {
                error = "Password cannot be null";
                return false;
            }

            Regex regex = new Regex(@"^(?=.*[0-9])(?=.*[!@#$%^&*])[0-9a-zA-Z!@#$%^&*0-9]{8,}$");
            Match match = regex.Match(txtPassword);
            if (!match.Success)
            {
                error = "The Password must have at least one numeric and one special character ";
                return false;
            }
            else
                return true;
        }
        public Boolean check_confirmpassword(string txtConfirmPassword)
        {
            if (txtConfirmPassword == "")
            {
                error = " Confirm Password cannot be null";
                return false;
            }
            else
                return true;
        }
       
        public Boolean compare_password(string password, string confirm)
        {

            if (confirm != password)
            {
                error = "confirm password not matching ";

                return false;
            }
            else
                return true;
        }
        public Boolean check_username(string txtusername)
        {

            if (txtusername == "")
            {
                error = "Username cannot be null ";

                return false;
            }
            Regex regex = new Regex("^[a-zA-Z0-9]+$");
            Match match = regex.Match(txtusername);
            if (!match.Success)
            {
                error = "Username should contain atleast one letter or number";
                return false;
            }
            else
                return true;
        }

    }
}