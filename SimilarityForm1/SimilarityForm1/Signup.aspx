﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Signup.aspx.cs" Inherits="SimilarityForm1.Signup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SimilarityForm | Signup</title>
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="gray-bg">
    <form id="form1" runat="server">
     <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">Signup</h1>

                <!-- <h1 class="logo-name">Signup</h1> -->

            </div>
            <h3>&nbsp;</h3>
            <h3>Register</h3>
            
          
                <div class="form-group">
                    <asp:TextBox ID="txt_name" class="form-control" placeholder="Name" runat="server"></asp:TextBox>
                    <asp:Label ID="labelname" runat="server" Text="Label"></asp:Label>
                </div>
                <div class="form-group">
                      <asp:TextBox ID="txt_uname" class="form-control" placeholder="Username"   runat="server"></asp:TextBox>
                     <asp:Label ID="labeluname" runat="server" Text="Label"></asp:Label>
                </div>
                <div class="form-group">
                      <asp:TextBox ID="txt_pass" class="form-control" placeholder="Password" runat="server"></asp:TextBox>
                   <asp:Label ID="labelpass" runat="server" Text="Label"></asp:Label>
                </div>

             <div class="form-group">
                      <asp:TextBox ID="txt_confirm" class="form-control" placeholder="Confirm Password"  runat="server"></asp:TextBox>
                   <asp:Label ID="labelconfirm" runat="server" Text="Label"></asp:Label>

                   
              
                </div>
           
                <p class="text-muted text-center">&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" onclick="Button3_Click1" 
                        Text="Register" Width="231px" BackColor="#FF9999" />
            </p>
            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <asp:Button ID="Button2" runat="server" Height="24px" onclick="Button2_Click" 
                Text="Signin" Width="226px" BackColor="White" />
&nbsp;</div>
    </div>
    <script src="js/bootstrap.js" type="text/javascript"></script>
    <script src="js/icheck.min.js" type="text/javascript"></script>
    <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="js/popper.min.js" type="text/javascript"></script>
    </form>
</body>
</html>
