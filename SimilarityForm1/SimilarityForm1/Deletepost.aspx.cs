﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SimilarityForm1
{
    public partial class Deletepost : System.Web.UI.Page
    {
        public Class1 obb;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh();
            }
        }

        public void refresh()
        {
            obb = new Class1();
            obb.ExecuteQuery("Select * from Upload where Owner='" + Session["Name"].ToString() + "'");

            rptData.DataSource = obb.DT;
            rptData.DataBind();

            if (obb.DT.Rows.Count > 0)
                hfTrue.Value = "1";
            else
                hfTrue.Value = "0";
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");

        }
        protected void Button1_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                obb = new Class1();
                obb.ExecuteNonQuery("delete from Upload where PId='" + e.CommandArgument.ToString().Trim() + "'");
                refresh();
            }

        }
        protected void rptData_ItemCommand1(object source, RepeaterCommandEventArgs e)
        {

        }
    }
}